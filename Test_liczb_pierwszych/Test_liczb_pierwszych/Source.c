#include "data.h"

int main()
{
	int n, k;

	printf("Podaj liczbe do sprawdzenia: ");
	scanf_s("%d", &n);

	printf("Podaj dokladnosc sprawdzenia: ");
	scanf_s("%d", &k);

	if (Miller_Rabin(n, k) == 1)
	{
		printf("Liczba jest prawdopodobnie pierwsza.\n");
	}
	else
	{
		printf("Liczba jest zlozona.\n");
	}

	system("PAUSE");
	return EXIT_SUCCESS;

}
