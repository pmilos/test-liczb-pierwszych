#include "data.h"

long long int potegowanie_modulo(long long int a,long long int b,long long int m)
{
	int i;
	long long int wynik = 1;
	long long int x = a%m;

	for (i = 1; i <= b; i <<= 1)
	{
		x %= m;
		if ((b&i) != 0)
		{
			wynik *= x;
			wynik %= m;
		}
		x *= x;
	}

	return wynik;
}