#include "data.h"

long long int Miller_Rabin(long long int n,long long int k)
{

	int potegi_2[31] =
	{ 1 << 0, 1 << 1, 1 << 2, 1 << 3, 1 << 4, 1 << 5, 1 << 6,
	1 << 7, 1 << 8, 1 << 9, 1 << 10, 1 << 11, 1 << 12, 1 << 13,
	1 << 14, 1 << 15, 1 << 16, 1 << 17, 1 << 18, 1 << 19, 1 << 20,
	1 << 21, 1 << 22, 1 << 23, 1 << 24, 1 << 25, 1 << 26, 1 << 27,
	1 << 28, 1 << 29, 1 << 30 };

	long long int s = 0;
	long long int s2 = 1;
	long long int a, d, i, r, liczba_pierwsza;
	srand((unsigned)time(NULL));

	if (n<4)
	{
		return 1;
	}
	if (n % 2 == 0)
	{
		return 0;
	}

	// Obliczanie S i D
	while ((s2 & (n - 1)) == 0)
	{
		s += 1;
		s2 <<= 1;
	}
	d = n / s2;

	// Powtarzanie k razy
	for (i = 0; i<k; i++)
	{
		a = 1 + (long long int)((n - 1)*rand() / (RAND_MAX + 1.0));
		if (potegowanie_modulo(a, d, n) != 1)
		{
			liczba_pierwsza = 0;
			for (r = 0; r <= s - 1; r++)
			{
				if (potegowanie_modulo(a, potegi_2[r] * d, n) == n - 1)
				{
					liczba_pierwsza = 1;
					break;
				}
			}
			if (liczba_pierwsza == 0)
			{
				return 0;
			}
		}
	}

	return 1;
}